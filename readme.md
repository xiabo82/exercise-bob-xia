#iOS Candidate Exercise

##Overview
To verify iOS knowledge and general development skill we ask that candidates complete this small exercise.  Your work will be evaluated primarily on the code, but also on the built product.

The exercise should be completed as if you were submitting code to a larger project.  It should showcase your ability as a professional iOS developer.

##Tasks
You should have received a repository for a basic iPhone application.  The application allows you to search domain.com.au for properties for purchase in Pyrmont with either 1+, 2+ or 3+ bedrooms.

You are tasked with:

* Addressing a UX issue: Currently it can take quite a while before the user is presented with results after tapping the "Search" button.  We're not sure why this is, but it would be great if the user recieved some kind of feedback sooner.
* Implementing a new feature: We'd like to add something like the standard swipe-to-delete functionality of iOS to the results table view.  
    * The word "Ignore" should be used in place of the default "Delete" on the standard swipe-to-delete control.
    * Instead of removing the listing, it should change to a smaller, 'greyed-out' style (feel free to be creative). Tapping this listing will return it to the full display.
    * Ignored properties are to be persisted, and presented as ignored when a fresh search is conducted. The field "AdId" uniquely identifies a listing.

* The code is **not** what we would consider _production quality_.  We may have even inserted a few bugs on purpose, sorry about that.
    * Feel free to refactor in order to complete the tasks above.
    * Feel free to improve the code quality wherever you see fit, even if not necessary to achieve the tasks above.

##Guidelines
* Support iOS6 and above
* Feel free to make use of any open source libraries you feel are of production quality.
* The project is to be submitted as a git repository with history. It can be submitted either as a zip or as a private repository hosted on [bitbucket.org](www.bitbucket.org)