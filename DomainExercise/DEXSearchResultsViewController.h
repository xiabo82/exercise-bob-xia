//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import <UIKit/UIKit.h>

@interface DEXSearchResultsViewController : UIViewController

- (id)initWithResults:(NSArray *)results;

@end
