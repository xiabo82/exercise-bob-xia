//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import <UIKit/UIKit.h>

@interface DEXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
