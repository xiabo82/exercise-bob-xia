//  Copyright (c) 2013 Fairfax Digital. All rights reserved.

#import "DEXBackgroundColoredView.h"


@implementation DEXBackgroundColoredView

- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame backgroundColor:nil];
}

- (id)initWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor {
    if ((self = [super initWithFrame:frame])) {
        [super setBackgroundColor:backgroundColor];
    }
    return self;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
}

- (void)bcv_setBackgroundColor:(UIColor *)color {
    [super setBackgroundColor:color];
}

@end
