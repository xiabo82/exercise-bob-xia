//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import "DEXSearchResultPlainCell.h"

#import "FMGeometry.h"
#import "DEXBackgroundColoredView.h"
#import "DEXListingDataHelper.h"


static UIEdgeInsets const DEXSearchResultPlainCellPadding = (UIEdgeInsets){ .top = 6, .right = 10, .bottom = 6, .left = 6 };

static CGFloat const DEXSearchResultPlainCellInnerPaddingLength = 8;
static CGFloat const DEXSearchResultPlainCellTextPaddingLength = 3;

static CGSize const DEXSearchResultPlainCellImageSize = (CGSize){ .width = 150, .height = 100 };

@interface DEXSearchResultPlainCell ()

@property (nonatomic,strong,readonly) UILabel *amenitiesLabel;
@property (nonatomic,strong,readonly) UILabel *addressLabel;
@property (nonatomic,strong,readonly) UILabel *priceLabel;

@property (nonatomic,strong,readonly) UIImageView *thumbnailImageView;

@property (nonatomic,strong,readonly) UIView *strokeLowerView;

@end


@implementation DEXSearchResultPlainCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        self.backgroundColor = [UIColor clearColor];
        self.contentMode = UIViewContentModeRedraw;
        
        UIView * const contentView = self.contentView;
        contentView.backgroundColor = [UIColor whiteColor];
        
        _thumbnailImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
        _thumbnailImageView.clipsToBounds = YES;
        
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _priceLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
        _priceLabel.textColor = [UIColor colorWithRed:0x99/255. green:0x33/255. blue:0x66/255. alpha:1];
        _priceLabel.numberOfLines = 2;
        _priceLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _priceLabel.backgroundColor = [UIColor clearColor];
        
        _amenitiesLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _amenitiesLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        _amenitiesLabel.textColor = [UIColor colorWithWhite:0x33/255. alpha:1];
        _amenitiesLabel.backgroundColor = [UIColor clearColor];
        
        _addressLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _addressLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
        _addressLabel.textColor = [UIColor colorWithWhite:0x77/255. alpha:1];
        _addressLabel.numberOfLines = 3;
        _addressLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _addressLabel.backgroundColor = [UIColor clearColor];
        
        UIColor * const strokeColor = [UIColor colorWithWhite:0 alpha:55./255.];
        _strokeLowerView = [[DEXBackgroundColoredView alloc] initWithFrame:CGRectZero backgroundColor:strokeColor];
        
        [contentView addSubview:_strokeLowerView];
        
        [contentView addSubview:_thumbnailImageView];
        [contentView addSubview:_priceLabel];
        [contentView addSubview:_amenitiesLabel];
        [contentView addSubview:_addressLabel];
    }
    return self;
}

+ (CGFloat)height {
    return DEXSearchResultPlainCellPadding.top + DEXSearchResultPlainCellImageSize.height + DEXSearchResultPlainCellPadding.bottom;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self setListing:nil];
    [self.thumbnailImageView setImage:nil];
}

- (void)setListing:(NSDictionary *)listing {
    self.priceLabel.text = DEXPriceDisplayTextForListing(listing);
    self.amenitiesLabel.text = DEXAmenitiesTextForListing(listing);
    self.addressLabel.text = listing[@"DisplayableAddress"];
    self.thumbnailImageView.image = listing[@"ThumbUrl"];
    
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    UIWindow * const window = self.window;
    UIScreen * const screen = window.screen;
    CGFloat const scale = screen.scale ?: 1;
    
    CGRect const cellBounds = self.bounds;
    UIView * const contentView = self.contentView;
    UIView * const backgroundView = self.backgroundView;
    UIView * const selectedBackgroundView = self.selectedBackgroundView;
    
    {
        CGRect contentViewFrame = contentView.frame;
        contentViewFrame.size = cellBounds.size;
        contentView.frame = contentViewFrame;
    }
    CGRect const bounds = contentView.bounds;
    backgroundView.frame = bounds;
    selectedBackgroundView.frame = bounds;
    
    UIImageView * const thumbnailImageView = self.thumbnailImageView;
    UILabel * const priceLabel = self.priceLabel;
    UILabel * const amenitiesLabel = self.amenitiesLabel;
    UILabel * const addressLabel = self.addressLabel;
    
    UIView * const strokeLowerView = self.strokeLowerView;
    
    CGFloat const strokeHeight = 1 / scale;
    
    CGRect const strokeLowerRect = FMRectDivide(bounds, strokeHeight, CGRectMaxYEdge);
    
    CGRect scratch = UIEdgeInsetsInsetRect(bounds, DEXSearchResultPlainCellPadding);
    CGRect const thumbnailImageViewRect = FMRectDivide(scratch, &scratch, DEXSearchResultPlainCellImageSize.width, CGRectMinXEdge);
    (void)FMRectDivide(scratch, &scratch, DEXSearchResultPlainCellInnerPaddingLength, CGRectMinXEdge);
    CGRect const contentTextRect = CGRectInset(scratch, 0, DEXSearchResultPlainCellTextPaddingLength);
    
    CGFloat const contentTextRectWidth = CGRectGetWidth(contentTextRect);
    CGFloat contentTextRectRemainingHeight = CGRectGetHeight(contentTextRect);
    CGFloat const priceLabelMaxWidth = contentTextRectWidth - DEXSearchResultPlainCellTextPaddingLength;
    CGSize const priceLabelSize = FMSizeCeil([priceLabel sizeThatFits:(CGSize){ .width = priceLabelMaxWidth, .height = MIN(contentTextRectRemainingHeight, 60) }]);
    if (priceLabelSize.height) {
        contentTextRectRemainingHeight -= priceLabelSize.height;
        contentTextRectRemainingHeight -= DEXSearchResultPlainCellTextPaddingLength;
    }
    CGSize const amenitiesLabelSize = FMSizeCeil([amenitiesLabel.text sizeWithFont:amenitiesLabel.font constrainedToSize:(CGSize){ .width = contentTextRectWidth, .height = MIN(contentTextRectRemainingHeight, 30) } lineBreakMode:amenitiesLabel.lineBreakMode]);
    if (amenitiesLabelSize.height > 0) {
        contentTextRectRemainingHeight -= amenitiesLabelSize.height;
        contentTextRectRemainingHeight -= DEXSearchResultPlainCellTextPaddingLength;
    }
    CGSize const addressLabelSize = FMSizeCeil([addressLabel.text sizeWithFont:addressLabel.font constrainedToSize:(CGSize){ .width = contentTextRectWidth, .height = MIN(contentTextRectRemainingHeight, 60) } lineBreakMode:addressLabel.lineBreakMode]);
    if (addressLabelSize.height) {
        contentTextRectRemainingHeight -= addressLabelSize.height;
        contentTextRectRemainingHeight -= DEXSearchResultPlainCellTextPaddingLength;
    }
    
    scratch = contentTextRect;
    CGRect priceLabelRect = FMRectDivide(scratch, &scratch, priceLabelSize.height, CGRectMinYEdge);
    priceLabelRect.size.width = priceLabelMaxWidth;
    if (priceLabelSize.height) {
        (void)FMRectDivide(scratch, &scratch, DEXSearchResultPlainCellTextPaddingLength, CGRectMinYEdge);
    }
    CGRect const amenitiesViewRect = FMRectDivide(scratch, &scratch, amenitiesLabelSize.height, CGRectMinYEdge);
    if (amenitiesLabelSize.height > 0) {
        (void)FMRectDivide(scratch, &scratch, DEXSearchResultPlainCellTextPaddingLength, CGRectMinYEdge);
    }
    CGRect const addressLabelRect = FMRectDivide(scratch, &scratch, addressLabelSize.height, CGRectMinYEdge);

    thumbnailImageView.frame = thumbnailImageViewRect;
    priceLabel.frame = priceLabelRect;
    amenitiesLabel.frame = amenitiesViewRect;
    addressLabel.frame = addressLabelRect;
    
    strokeLowerView.frame = strokeLowerRect;
}

@end
