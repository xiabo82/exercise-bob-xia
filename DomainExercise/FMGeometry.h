//  Copyright (c) 2012 Fairfax Media. All rights reserved.


#ifndef FMCoreGraphics_FMGeometry_h
#define FMCoreGraphics_FMGeometry_h

#include <Foundation/Foundation.h>
#include <CoreGraphics/CoreGraphics.h>


/**
 Gives a CGPoint integral coordinates by flooring them.
 
 @param point The CGPoint to integerise
 
 @return a floored `point`
 */
extern CGPoint __attribute__((warn_unused_result)) FMPointIntegral(CGPoint point);


/**
 A NULL-safe CGRectDivide
 
 @param rect the containing CGRect
 @param slice a possibly-NULL pointer to a CGRect
 @param remainder a possibly-NULL pointer to a CGRect
 @param amount the amount to slice off `rect`
 @param edge the edge to take `slice` from
 */
extern void __attribute__((overloadable)) FMRectDivide(CGRect rect, CGRect *slice, CGRect *remainder, CGFloat amount, CGRectEdge edge);


/**
 A convenience override of FMRectDivide that returns the slice
 
 @param rect the containing CGRect
 @param remainder a possibly-NULL pointer to a CGRect
 @param amount the amount to slice off `rect`
 @param edge the edge to take `slice` from
 
 @return the slice
 */
extern CGRect __attribute__((overloadable,warn_unused_result)) FMRectDivide(CGRect rect, CGRect *remainder, CGFloat amount, CGRectEdge edge);


/**
 A convenience override of FMRectDivide that returns the slice and ignores the remainder
 
 @param rect the containing CGRect
 @param amount the amount to slice off `rect`
 @param edge the edge to take `slice` from
 
 @return the slice
 */
extern CGRect __attribute__((overloadable,warn_unused_result)) FMRectDivide(CGRect rect, CGFloat amount, CGRectEdge edge);


/**
 @enum FMRectAxes
 
 @discussion The FMRectAxes enum defines constants for X and Y axes.
 */
typedef NS_OPTIONS(unsigned int, FMRectAxes) {
    FMRectAxisX = 0x1,
    FMRectAxisY = 0x2,
};


/**
 Centers a rect within a containing rect
 
 @param containerRect the containing rect
 @param rect a rect to center withing `containerRect`
 @param axes the axes along which to center `rect`. Values are from FMRectAxisOptions
 
 @return the centered CGRect
 */
extern CGRect __attribute__((warn_unused_result)) FMRectCenter(CGRect containerRect, CGRect rect, FMRectAxes axes);


/**
 @enum FMRectAlignment
 
 @discussion The FMRectAlignment enum defines constants used to determine the alignment of a CGRect in relation to another CGRect.
 */
typedef NS_OPTIONS(unsigned int, FMRectAlignment) {
    FMRectAlignmentXLeft          = 0x001,
    FMRectAlignmentXRight         = 0x002,
    FMRectAlignmentXCenter        = 0x003,
    FMRectAlignmentYTop           = 0x010,
    FMRectAlignmentYBottom        = 0x020,
    FMRectAlignmentYCenter        = 0x030,
    FMRectAlignmentOriginIntegral = 0x100,
    FMRectAlignmentXInside        = 0x200,
    FMRectAlignmentYInside        = 0x400,
};
static unsigned int const FMRectAlignmentInside = FMRectAlignmentXInside|FMRectAlignmentYInside;


/**
 Aligns a rect within a containing rect
 
 @param containerRect the containing rect
 @param rect a CGRect to center within `containerRect`
 @param alignment a bitfield indicating which alignments to make. Values to be used are from FMRectAlignmentOptions
 
 @return an aligned CGRect
 */
extern CGRect __attribute__((overloadable,warn_unused_result)) FMRectAlign(CGRect containerRect, CGRect rect, FMRectAlignment alignment);

/**
 Computes a rect of the given size aligned within a containing rect
 
 @param containerRect the containing rect
 @param size the size of the rect to compute within `containerRect`
 @param alignment a bitfield indicating which alignments to make
 
 @return an aligned CGRect
 */
extern CGRect __attribute__((overloadable,warn_unused_result)) FMRectAlign(CGRect containerRect, CGSize size, FMRectAlignment alignment);

/**
 Calculates a transform to convert between two CGRects
 
 @param initialRect the starting rect
 @param targetRect the final rect
 */
extern CGAffineTransform __attribute__((warn_unused_result)) FMAffineTransformMakeWithInitialRectAndTargetRect(CGRect initialRect, CGRect targetRect);


/**
 Ceils both components of a CGSize

 @param size the CGSize to ceil

 @return a ceiled CGSize
 */
static inline CGSize FMSizeCeil(CGSize size) {
    return (CGSize){
        .width = ceil(size.width),
        .height = ceil(size.height),
    };
}

/**
 Scales a CGSize down if necessary to fit inside another, preserving its aspect ratio

 @param containerSize the size of the container to fit inside
 @param size the size to be scaled

 @return a CGSize that wholly fits inside `containerSize`, with equal or smaller dimensions as `size` and the same aspect ratio
 */
extern CGSize __attribute__((warn_unused_result)) FMSizeScaleDownAspectFit(CGSize containerSize, CGSize size);

/**
 Scales a CGSize by a constant factor

 @param size the size to be scaled
 @param scale the scale factor to apply

 @return a CGSize with dimensions multiplied by scale
 */
static inline CGSize __attribute__((warn_unused_result)) FMSizeScale(CGSize size, CGFloat scale) {
    return (CGSize){
        .width = size.width * scale,
        .height = size.height * scale,
    };
}

#endif
