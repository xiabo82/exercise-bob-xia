//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import <UIKit/UIKit.h>

#import "DEXAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DEXAppDelegate class]));
    }
}
