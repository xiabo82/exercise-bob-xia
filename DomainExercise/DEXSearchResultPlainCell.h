//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import <UIKit/UIKit.h>

@interface DEXSearchResultPlainCell : UITableViewCell

- (void)setListing:(NSDictionary *)listing;

+ (CGFloat)height;

@end
