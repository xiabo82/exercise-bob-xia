//  Copyright (c) 2013 Fairfax Digital. All rights reserved.

#import <UIKit/UIKit.h>


@interface DEXBackgroundColoredView : UIView
- (id)initWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor;
- (void)bcv_setBackgroundColor:(UIColor *)color;
@end
