//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import "DEXSearchViewController.h"

#import "FMGeometry.h"

#import "DEXSearchResultsViewController.h"

@interface DEXSearchViewController ()
@property(nonatomic,weak) UISegmentedControl *bedsSegmentedControl;
@end

@implementation DEXSearchViewController

- (id)init {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.title = @"Search";
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)loadView {
    UIView * const view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view = view;
    view.backgroundColor = [UIColor colorWithRed:232./255. green:232./255. blue:223./255. alpha:1];
    
    UISegmentedControl * const bedsSegmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"1+", @"2+", @"3+"]];
    self.bedsSegmentedControl = bedsSegmentedControl;
    CGSize const bedsSegmentedControlSize = (CGSize){.width=300,.height=CGRectGetHeight(bedsSegmentedControl.bounds)};
    bedsSegmentedControl.frame = FMRectAlign(view.bounds, bedsSegmentedControlSize, FMRectAlignmentXCenter|FMRectAlignmentYCenter|FMRectAlignmentOriginIntegral);
    [view addSubview:bedsSegmentedControl];
    
    CGRect const bedsLabelFrame = CGRectOffset(bedsSegmentedControl.frame, 0, -CGRectGetHeight(bedsSegmentedControl.bounds));
    UILabel * const bedsLabel = [[UILabel alloc] initWithFrame:bedsLabelFrame];
    bedsLabel.text = @"Bedrooms";
    bedsLabel.font = [UIFont systemFontOfSize:18];
    bedsLabel.textAlignment = NSTextAlignmentCenter;
    bedsLabel.backgroundColor = [UIColor clearColor];
    [view addSubview:bedsLabel];

    UIImage * const searchButtonImage = [[UIImage imageNamed:@"SubmitButton"] resizableImageWithCapInsets:(UIEdgeInsets){ .top = 5, .right = 5, .bottom = 5, .left = 5}];
    CGSize const searchButtonSize = (CGSize){ .width = CGRectGetWidth(view.bounds) - 20, .height = searchButtonImage.size.height };
    CGRect const searchButtonContainer = FMRectDivide(view.bounds, searchButtonSize.height+10, CGRectMaxYEdge);
    CGRect const searchButtonFrame = FMRectAlign(searchButtonContainer, searchButtonSize, FMRectAlignmentXCenter|FMRectAlignmentYTop);
    UIButton * const searchButton = [[UIButton alloc] initWithFrame:searchButtonFrame];
    searchButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [searchButton setBackgroundImage:searchButtonImage forState:UIControlStateNormal];
    [searchButton setTitle:@"Search" forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:searchButton];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.bedsSegmentedControl setSelectedSegmentIndex:0];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Actions
- (void)searchButtonTapped:(id)sender {
    __weak __typeof(self) wself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __typeof(self) sself = wself;
        NSString * const urlString = [NSString stringWithFormat:@"https://rest.domain.com.au/searchservice.svc/search?areas=Sydney%%20City&bedrooms=%%3E%d&mode=buy&pcodes=2009&regions=Sydney%%20Region&results=200&searchtype=1&state=NSW&sub=Pyrmont", sself.bedsSegmentedControl.selectedSegmentIndex+1];
        NSURL * const searchURL = [NSURL URLWithString:urlString];
        NSData * const responseData = [NSData dataWithContentsOfURL:searchURL];
        NSDictionary * const responseObject = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:NULL];
        
        NSArray * const listings = responseObject[@"ListingResults"][@"Listings"];
        for ( NSMutableDictionary *listing in listings) {
            NSData  *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:listing[@"ThumbUrl"]]];
            listing[@"ThumbUrl"] = [UIImage imageWithData:imageData];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            DEXSearchResultsViewController *viewController = [[DEXSearchResultsViewController alloc] initWithResults:listings];
            [wself.navigationController pushViewController:viewController animated:YES];
        });
    });
}

@end
