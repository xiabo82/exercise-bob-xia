//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import <Foundation/Foundation.h>

extern NSString *DEXPriceDisplayTextForListing(NSDictionary *listing);
extern NSString *DEXAmenitiesTextForListing(NSDictionary *listing);
