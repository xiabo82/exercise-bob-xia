//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import "DEXListingDataHelper.h"

NSString *DEXPriceDisplayTextForListing(NSDictionary *listing) {
    NSString *displayPrice = listing[@"DisplayPrice"];
    if (displayPrice.length == 0) {
        return @"Contact Agent";
    }
    
    return displayPrice;
}

extern NSString *DEXAmenitiesTextForListing(NSDictionary *listing) {
    NSMutableArray * const amenityTokens = [[NSMutableArray alloc] initWithCapacity:3];
    
    NSInteger numberOfBeds = [listing[@"Bedrooms"] integerValue];
    NSInteger numberOfBaths = [listing[@"Bathrooms"] integerValue];
    NSInteger numberOfParkingSpaces = [listing[@"Carspaces"] integerValue];
    
    if (numberOfBeds == 0) {
    } else if (numberOfBeds == 1) {
        [amenityTokens addObject:@"1 bed"];
    } else {
        [amenityTokens addObject:[NSString stringWithFormat:@"%ld beds", (long)numberOfBeds]];
    }
    
    if (numberOfBaths == 0) {
    } else if (numberOfBaths == 1) {
        [amenityTokens addObject:@"1 bath"];
    } else {
        [amenityTokens addObject:[NSString stringWithFormat:@"%ld baths", (long)numberOfBaths]];
    }
    
    if (numberOfParkingSpaces == 0) {
    } else if (numberOfParkingSpaces == 1) {
        [amenityTokens addObject:@"1 parking"];
    } else {
        [amenityTokens addObject:[NSString stringWithFormat:@"%ld parking", (long)numberOfParkingSpaces]];
    }
    
    return [amenityTokens componentsJoinedByString:@",\u2002"];
}
