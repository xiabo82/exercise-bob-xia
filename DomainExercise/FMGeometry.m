//  Copyright (c) 2012 Fairfax Media. All rights reserved.

#import "FMGeometry.h"

#include <math.h>


CGPoint FMPointIntegral(CGPoint point) {
    point.x = floorf(point.x);
    point.y = floorf(point.y);
    return point;
}

void __attribute__((overloadable)) FMRectDivide(const CGRect rect, CGRect *slice, CGRect *remainder, const CGFloat amount, const CGRectEdge edge) {
    CGRect throwaway = CGRectZero;
    if (!slice) {
        slice = &throwaway;
    }
    if (!remainder) {
        remainder = &throwaway;
    }
    CGRectDivide(rect, slice, remainder, amount, edge);
}

CGRect __attribute__((overloadable)) FMRectDivide(CGRect rect, CGRect *remainder, CGFloat amount, CGRectEdge edge) {
    CGRect slice;
    FMRectDivide(rect, &slice, remainder, amount, edge);
    return slice;
}

CGRect __attribute__((overloadable)) FMRectDivide(CGRect rect, CGFloat amount, CGRectEdge edge) {
    return FMRectDivide(rect, NULL, amount, edge);
}

CGRect FMRectCenter(CGRect containerRect, CGRect rect, FMRectAxes axes) {
    if (axes & FMRectAxisX) {
        rect.origin.x = CGRectGetMidX(containerRect) - CGRectGetWidth(rect) / 2.f;
    }
    if (axes & FMRectAxisY) {
        rect.origin.y = CGRectGetMidY(containerRect) - CGRectGetHeight(rect) / 2.f;
    }
    return rect;
}

static const unsigned int FMRectAlignmentMaskX = 0x0f;
static const unsigned int FMRectAlignmentMaskY = 0xf0;

CGRect __attribute__((overloadable)) FMRectAlign(CGRect containerRect, CGRect rect, FMRectAlignment alignment) {
    switch (alignment & FMRectAlignmentMaskX) {
        case FMRectAlignmentXLeft:
            rect.origin.x = CGRectGetMinX(containerRect);
            break;
        case FMRectAlignmentXCenter:
            rect.origin.x = CGRectGetMidX(containerRect) - CGRectGetWidth(rect) / 2.f;
            break;
        case FMRectAlignmentXRight:
            rect.origin.x = CGRectGetMaxX(containerRect) - CGRectGetWidth(rect);
            break;
    }
    switch (alignment & FMRectAlignmentMaskY) {
        case FMRectAlignmentYTop:
            rect.origin.y = CGRectGetMinY(containerRect);
            break;
        case FMRectAlignmentYCenter:
            rect.origin.y = CGRectGetMidY(containerRect) - CGRectGetHeight(rect) / 2.f;
            break;
        case FMRectAlignmentYBottom:
            rect.origin.y = CGRectGetMaxY(containerRect) - CGRectGetHeight(rect);
            break;
    }

    if (alignment & FMRectAlignmentXInside) {
        if (rect.size.width > containerRect.size.width) {
            rect.size.width = containerRect.size.width;
        }
        if (CGRectGetMinX(rect) < CGRectGetMinX(containerRect)) {
            rect.origin.x = CGRectGetMinX(containerRect);
        } else if (CGRectGetMaxX(rect) > CGRectGetMaxX(containerRect)) {
            rect.origin.x = CGRectGetMaxX(containerRect) - rect.size.width;
        }
    }

    if (alignment & FMRectAlignmentYInside) {
        if (rect.size.height > containerRect.size.height) {
            rect.size.height = containerRect.size.height;
        }
        if (CGRectGetMinY(rect) < CGRectGetMinY(containerRect)) {
            rect.origin.y = CGRectGetMinY(containerRect);
        } else if (CGRectGetMaxY(rect) > CGRectGetMaxY(containerRect)) {
            rect.origin.y = CGRectGetMaxY(containerRect) - rect.size.height;
        }
    }
    
    if (alignment & FMRectAlignmentOriginIntegral) {
        rect.origin = FMPointIntegral(rect.origin);
    }
    
    return rect;
}

CGRect __attribute__((overloadable)) FMRectAlign(CGRect containerRect, CGSize size, FMRectAlignment alignment) {
    CGRect rect = (CGRect){CGPointZero,size};
    return FMRectAlign(containerRect, rect, alignment);
}

CGAffineTransform FMAffineTransformMakeWithInitialRectAndTargetRect(CGRect initialRect, CGRect targetRect) {
    // ignore invalid rects, empty rects could be valid though
    if (CGRectIsNull(initialRect) || CGRectIsNull(targetRect)) {
        return CGAffineTransformIdentity;
    }
    
    CGPoint const initialRectCenter = CGPointMake(CGRectGetMidX(initialRect), CGRectGetMidY(initialRect));
    CGSize const initialRectSize = initialRect.size;
    CGPoint const targetRectCenter = CGPointMake(CGRectGetMidX(targetRect), CGRectGetMidY(targetRect));
    CGSize const targetRectSize = targetRect.size;
    
    CGFloat const translateX = targetRectCenter.x - initialRectCenter.x;
    CGFloat const translateY = targetRectCenter.y - initialRectCenter.y;
    
    CGFloat const scaleX = (initialRectSize.width > 0) ? (targetRectSize.width / initialRectSize.width) : CGFLOAT_MAX;
    CGFloat const scaleY = (initialRectSize.height > 0) ? (targetRectSize.height / initialRectSize.height) : CGFLOAT_MAX;
    
    return CGAffineTransformMake(scaleX, 0, 0, scaleY, translateX, translateY);
}


CGSize __attribute__((warn_unused_result)) FMSizeScaleDownAspectFit(CGSize containerSize, CGSize size) {
    NSCParameterAssert(isfinite(size.width));
    NSCParameterAssert(isfinite(size.height));
    if (!isfinite(size.width) || !isfinite(size.height)) {
        return size;
    }

    CGFloat const containerWidth = fabs(containerSize.width);
    CGFloat const containerHeight = fabs(containerSize.height);
    CGFloat const originalWidth = fabs(size.width);
    CGFloat const originalHeight = fabs(size.height);

    double const ratioWidth = (containerWidth > 0) ? originalWidth / containerWidth : INFINITY;
    double const ratioHeight = (containerHeight > 0) ? originalHeight / containerHeight : INFINITY;

    if (!isfinite(ratioWidth) && !isfinite(ratioHeight)) {
        return size;
    }

    double const ratioLargest = !isfinite(ratioWidth) ? ratioHeight : MAX(ratioWidth, ratioHeight);
    if (ratioLargest <= 1) {
        return size;
    }

    return (CGSize){ .width = originalWidth / ratioLargest, .height = originalHeight / ratioLargest };
}
