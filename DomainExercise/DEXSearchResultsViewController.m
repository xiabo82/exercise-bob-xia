//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import "DEXSearchResultsViewController.h"

#import "DEXSearchResultPlainCell.h"

@interface DEXSearchResultsViewController () <UITableViewDataSource>
@property(nonatomic, strong) NSArray *results;
@property(nonatomic, weak) UITableView *tableView;
@end

@implementation DEXSearchResultsViewController

- (id)initWithResults:(NSArray *)results {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _results = results;
        
        self.title = @"Results";
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)loadView {
    UIView * const view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    self.view = view;
    
    UITableView * const tableView = [[UITableView alloc] initWithFrame:view.frame style:UITableViewStylePlain];
    self.tableView = tableView;
    self.tableView.clipsToBounds = NO;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.dataSource = self;
    tableView.rowHeight = [DEXSearchResultPlainCell height];
    [view addSubview:tableView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.results count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * const kCellIdentifier = @"ResultCell";
    DEXSearchResultPlainCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (!cell) {
        cell = [[DEXSearchResultPlainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
    }

    [cell setListing:self.results[indexPath.row]];
    return cell;
}

@end
