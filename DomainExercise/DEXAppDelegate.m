//  Copyright (c) 2014 Fairfax Media Pty. Ltd. All rights reserved.

#import "DEXAppDelegate.h"

#import "DEXSearchViewController.h"

@implementation DEXAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[[DEXSearchViewController alloc] init]];
    if ([self.window respondsToSelector:@selector(setTintColor:)]) {
        [self.window setTintColor:[UIColor colorWithRed:0x76/255. green:0xA3/255. blue:0x1B/255. alpha:1.f]];
    }
    [self.window makeKeyAndVisible];
    return YES;
}

@end
